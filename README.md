# minimal-ts-boilerplate

Minimal lightweight typescript boilerplate for quick sketching and new projects. 
Ready to run in a browser through parcel built-in server.

## Startup:
```
yarn install;
yarn start;
```  
## Scripts:
```
"scripts": {
       "start": "parcel serve src/index.html --open",
       "serve": "parcel serve src/index.html",
       "build": "parcel build src/index.html",
       "watch": "parcel watch src/index.html"
 }
```

## Notes
Disable 'safe write' in WebStorm or similar features that mess with file saving/loading/caching
 to avoid problems with parcel watcher.

## Dependencies
* TypeScript
* TSLint
* Parcel

## License
MIT.

## Author
Konrad Kozioł

https://gitlab.com/cjbturin/